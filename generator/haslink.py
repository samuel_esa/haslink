# -*- coding: utf-8 -*-
import json
import os
import shutil
import re
import sys
from datetime import datetime

def formatContent(i, key, page, lang, pug):
	file = lang + '/' + key + '-' + str(i + 1) + ".pug"
	print("---" + file)

	#format content
	content = page[lang]["content"].replace("@image", "\n@image")
	content = content.replace("\n\n@video", "*****")
	content = content.replace("\n@video", "*****")
	content = content.replace("@video", "*****")
	content = content.replace("*****", "\n@video")
	content = content.replace("\n\n@url", "&&&&&")
	content = content.replace("\n@url", "&&&&&")
	content = content.replace("@url", "&&&&&")
	content = content.replace("&&&&&", "\n\n@url")
	content = content.replace("\n\n中題：", "$$$$$")
	content = content.replace("\n中題：", "$$$$$")
	content = content.replace("中題：", "$$$$$")
	content = content.replace("$$$$$", "\n\n中題：")
	content = content.replace("\n\n", "#####")
	content = content.replace("\n \n", "#####")
	content = content.replace(" \n", "\n")
	content = content.replace("\n", "\n\t\t\t\t\t\t\t\t\tbr\n\t\t\t\t\t\t\t\t\t| ")
	content = content.replace("#####", "\n\t\t\t\t\t\t\t\t\tbr\n\t\t\t\t\t\t\t\t\tbr\n\t\t\t\t\t\t\t\t\t| ")
	content = ".paragraph\n\t\t\t\t\t\t\t\t\t| " + content

	image_index = 1

	#image
	while("@image" in content):
		image_name = key + '-s' + str(i + 1) + '-' + str(image_index) + '.jpg'

		#check image type
		image_props = content.split("@image")

		if len(image_props) > 1:
			image_props = image_props[1]

			if len(image_props) > 0 and image_props[0] == '[':
				image_props = image_props.split(']')[0]
				image_props = image_props.replace("[", "")
				image_props = image_props.replace("]", "")
			else:
				image_props = ''
		else:
			image_props = ''

		postfix = '\n\t\t\t\t\t\t\t\t.paragraph\n\t\t\t\t\t\t\t\t\t| '

		description = image_props.replace(".click", "").replace(".small", "").replace(".lang", "").replace("|", "")

		if '.lang' in image_props:
		    image_name = key + '-s' + str(i + 1) + '-' + str(image_index) + '-' + lang + '.jpg'

		if '.click' in image_props:
			image_html = 'img(src="../images/' + image_name + '" @click="onClickImage(\'../images/' + image_name + '\', \'' + description + '\')" alt="' + description + '").center'
		else:
			image_html = 'img(src="../images/' + image_name + '" alt="").normal.center'

		if '.small' in image_props:
			image_html = image_html + '.small'

		if '.x-small' in image_props:
		    image_html = image_html + '.x-small'

		if '.full' in image_props:
		    image_html = image_html + '.full'

		if image_props == '':
			content = content.replace('@image', image_html + postfix, 1)
		else:
			content = content.replace('@image[' + image_props + ']', image_html + postfix, 1)

		image_index = image_index + 1

	content = content.replace("\t| img", "img")
	content = content.replace("\t\t\t\t\t\t\t\t\t| \n\t\t\t\t\t\t\t\t\tbr\n\t\t\t\t\t\t\t\t\tbr\n", "")
	content = content.replace(".paragraph\n\t\t\t\t\t\t\t\timg", "img")
	content = content.replace(".paragraph\n\t\t\t\t\t\t\t\t\t| img", "img")
	content = content.replace("br\n\t\t\t\t\t\t\t\t\tbr\n\t\t\t\t\t\t\t\timg", "br\n\t\t\t\t\t\t\t\timg")

	# video icon
	while("@video" in content):
		#check image type
		video_props = content.split("@video[")[1].split("]")[0]

		video_props = video_props.split("|")

		link = video_props[0]
		description = ""

		if len(video_props) == 2:
		    link = video_props[0]
		    description = video_props[1]

		content = re.sub('@video\[(.*)\]', 'a(href="' + link + '" target="_blank" rel="noopener noreferrer")\n\t\t\t\t\t\t\t\t\t\t.media-icon.video-icon\n\t\t\t\t\t\t\t\t\t\t| ' + description, content, 1)

	content = content.replace("| a(href", "a(href")
	content = content.replace(".media-icon.video-icon\n\t\t\t\t\t\t\t\t\tbr", ".media-icon.video-icon")

	# h2 title
	while("中題：" in content):
		sub_title = content.split("中題：")[1]
		sub_title = sub_title.split("\n")[0]
		sub_title = sub_title.split("\t")[0]
		sub_title = sub_title.split("@")[0]

		postfix = '\n\t\t\t\t\t\t\t\t.paragraph\n\t\t\t\t\t\t\t\t\t| '

		content = content.replace("中題：" + sub_title, "h2 " + sub_title.strip() + postfix)

	content = content.replace(".paragraph\n\t\t\t\t\t\t\t\t\t| h2", "h2")
	content = content.replace("\tbr\n\t\t\t\t\t\t\t\t\tbr\n\t\t\t\t\t\t\t\t\t| h2", "h2")
	content = content.replace(".paragraph\n\t\t\t\t\t\t\t\t\t| \n\t\t\t\t\t\t\t\th2", "h2")
	content = content.replace("| \n\t\t\t\t\t\t\t\t\tbr", "")
	content = content.replace(".paragraph\n\t\t\t\t\t\t\t\t\t| \n\t\t\t\t\t\t\t\t\tbr\n\t\t\t\t\t\t\t\timg", "img")

	# bold
	while("@bold" in content):
		text = content.split("@bold[")[1].split("]")[0]

		content = content.replace('@bold[' + text + ']', '\n\t\t\t\t\t\t\t\t\tstrong ' + text + '\n\t\t\t\t\t\t\t\t\t| ', 1)

	# italic
	while("@italic" in content):
		text = content.split("@italic[")[1].split("]")[0]

		content = content.replace('@italic[' + text + ']', '\n\t\t\t\t\t\t\t\t\ti ' + text + '\n\t\t\t\t\t\t\t\t\t| ', 1)

    # url
	while("@url" in content):
		url = content.split("@url[")[1].split("]")[0]
		url = url.split("|")
		text = url[0]
		link = url[1]

		content = re.sub('@url\[(.*)\]', 'a(href="' + link + '" target="_blank" rel="noopener noreferrer")\n\t\t\t\t\t\t\t\t\t\t| ' + text, content, 1)

		content = content.replace("| a(href", "a(href")

		content = content.replace('@url[' + text + '|' + link + ']', '\n\t\t\t\t\t\t\t\t\ta(href="' + link + '", target="blank") ' + text + '\n\t\t\t\t\t\t\t\t\t| ', 1)

	content = content.replace(".paragraph\n\t\t\t\t\t\t\t\t\t| \n\t\t\t\t\t\t\t\t\tbr\n\t\t\t\t\t\t\t\t\tbr\n", ".paragraph\n")

	html = pug

	html = html.replace("@title", page[lang]["title"])
	html = html.replace("@content", content)

	if key == 'inside-story':
		html = html.replace("@inside", '.tag\n\t\t\t\t\t\t\t\t\t.inside-icon.background-contain.icon')
	else:
		html = html.replace("@inside", "")

	html_file = open(file, "w", encoding='utf-8')
	html_file.write(html)
	html_file.close()

filename = 'data.json'

f = open(filename, "r", encoding='utf-8')
data = json.load(f)

now = datetime.now()
date_time = now.strftime("%Y%m%d %H%M%S")
#path = 'export ' + date_time

#os.mkdir(path)

template_tc = open("template_tc.pug", "r", encoding='utf-8')
pug_tc = template_tc.read()
template_en = open("template_en.pug", "r", encoding='utf-8')
pug_en = template_en.read()

home_tc = open("index_tc.pug", "r", encoding='utf-8')
index_tc = home_tc.read()
home_en = open("index_en.pug", "r", encoding='utf-8')
index_en = home_en.read()

os.chdir('../src/pages')

if os.path.exists('tc'):
    shutil.rmtree('tc')
if os.path.exists('en'):
    shutil.rmtree('en')
os.mkdir('tc')
os.mkdir('en')

for key, value in data.items():
	print(key)

	for i, page in enumerate(value):
		formatContent(i, key, page, "tc", pug_tc)
		formatContent(i, key, page, "en", pug_en)

	html_file = open("tc/index.pug", "w", encoding='utf-8')
	html_file.write(index_tc)
	html_file.close()

	html_file = open("en/index.pug", "w", encoding='utf-8')
	html_file.write(index_en)
	html_file.close()

f.close()
template_tc.close()
template_en.close()
