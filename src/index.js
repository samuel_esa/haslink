import _ from 'lodash';
import 'bootstrap';
import $ from 'jquery';
import Vue from 'vue'
import VueCookies from 'vue-cookies'

Vue.use(VueCookies)

import './styles/main.scss';
import './styles/responsive.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import share from './pages/component/share.vue'
import modal from './pages/component/modal.vue'
//import video_modal from './pages/component/video_modal.vue'
import counter from './pages/component/counter.vue'
import moment from 'moment'
import {disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks} from 'body-scroll-lock';

const percentage = [0.8, 1.0, 1.2]

const issue = '07 2022'

const sharing = {
    email: {
        en: 'https://www3.ha.org.hk/ehaslink/mail/en/set_email.asp?link=',
        tc: 'https://www3.ha.org.hk/ehaslink/mail/ch/set_email.asp?link='
    },
    facebook: {
        en: 'https://www.facebook.com/share.php?u=',
        tc: 'https://www.facebook.com/share.php?u='
    }
}

const side = [
    {
        tc_title: '同理心',
        en_title: 'Go the extra mile',
        image: 'side-01.jpg',
        link: 'empathy-1.html',
        bg_size: 'center',
    },
    {
        tc_title: '恆心',
        en_title: 'Persistence',
        image: 'side-02.jpg',
        link: 'persistence-1.html',
        bg_size: 'center',
    },
    {
        tc_title: '細心',
        en_title: 'Care',
        image: 'side-03.jpg',
        link: 'care-1.html',
        bg_size: 'center',
    },
    {
        tc_title: '好奇心',
        en_title: 'Curiosity',
        image: 'side-04.jpg',
        link: 'curiosity-1.html',
        bg_size: 'center',
    },
    {
        tc_title: '13優秀青年：如果我是……',
        en_title: '13 Young Achievers “If I were…”',
        image: 'side-05.jpg',
        link: '13-young-achieves-1.html',
        bg_size: 'center',
    }
]

const content = [
    {
        show: true,
        tc_title: '封面故事',
        en_title: 'Cover Story',
        prefix: 'cover-story-',
        page: [
            {
                tc_name: '一步一腳印',
                en_name: 'Every step counts'
            },
            {
                tc_name: '優異獎得奬名單',
                en_name: 'Merit Award List'
            },
            {
                tc_name: '評選小組',
                en_name: 'Selection Panel',
            }
        ]
    },
    {
        show: true,
        tc_title: '同理心',
        en_title: 'Empathy',
        prefix: 'empathy-',
        page: [
            {
                tc_name: '復康「重案組」助殘疾者重享家庭樂',
                en_name: 'The tertiary rehab centre helping patients regain the joy of family life',
            },
            {
                tc_name: '治理病童不忘初心',
                en_name: "Keeping the fire of passion burning when curing child patients",
            },
            {
                tc_name: '醫病首重醫人',
                en_name: "Healing a person is more than curing of a disease",
            },
            {
                tc_name: '將心比心 驅走負能量',
                en_name: "Walking in others’ shoes and battling negative energy",
            },
            {
                tc_name: '是護理 也是護心',
                en_name: "Healing body and soul",
            }
        ]
    },
    {
        show: true,
        tc_title: '恆心',
        en_title: 'Persistence',
        prefix: 'persistence-',
        page: [
            {
                tc_name: '「見慣『惡人』就不會再怕」',
                en_name: '“There is nothing to fear if you have enough experience.”',
            },
            {
                tc_name: '謝謝你一直都在',
                en_name: "Thank you for being here",
            },
            {
                tc_name: '持續學習 支援前線',
                en_name: "Keep learning to contribute as much as possible",
            },
        ]
    },
    {
        show: true,
        tc_title: '細心',
        en_title: 'Care',
        prefix: 'care-',
        page: [
            {
                tc_name: '成就不可能的任務',
                en_name: 'Making the impossible possible',
            },
            {
                tc_name: '社區疫苗接種中心「示範單位」',
                en_name: 'The role model of Community Vaccination Centres',
            },
            {
                tc_name: '術前揸Fit人',
                en_name: 'The boss of pre-operative surgery',
            },
            {
                tc_name: '時代巨變下的急症「守門人」',
                en_name: 'A gatekeeper in the frontline of emergency healthcare',
            },
            {
                tc_name: '探索大腦未至之境',
                en_name: 'Star Trek-loving surgeon explores final frontiers of neurosurgery',
            }
        ]
    },
    {
        show: true,
        tc_title: '好奇心',
        en_title: 'Curiosity',
        prefix: 'curiosity-',
        page: [
            {
                tc_name: '上下求變 智慧不是口號',
                en_name: 'A bottom-up approach to innovative hospital services”',
            },
            {
                tc_name: '破格源於耐心堅持',
                en_name: 'Breakthrough in patient care requires patience and perseverance',
            },
            {
                tc_name: '走在沙士與新冠抗疫前線',
                en_name: 'A fighter in the frontline of SARS and COVID-19',
            }
        ]
    },
    {
        show: true,
        tc_title: '13優秀青年：如果我是……',
        en_title: '13 Young Achievers “If I were…”',
        prefix: '13-young-achieves-',
        page: [
            {
                tc_name: '陳梓醫生/ 陳君裕/ 譚永輝醫生',
                en_name: 'Dr Chan Zi/ Chan Kwun-yu/ Dr Tam Anthony Raymond ',
            },
            {
                tc_name: '佘日峯醫生/ 蔡珊珊/ 陳宇軒醫生',
                en_name: 'Dr Shea Yat-fung/ Choi Shan-shan/ Dr Eugene Chan',
            },
            {
                tc_name: '馬師浩醫生/ 黃偉龍/ 鄭正禧醫生/ 林美瑩醫生',
                en_name: 'Dr Ma Sze-ho/ Jacob Wong/ Dr Cheng James Wesley Ching-hei/ Dr Lim Mei-ying',
            },
            {
                tc_name: '周穎思醫生/ 廖匡平/ 羅欣珮醫生',
                en_name: 'Dr Vanissa Chow/ Liu Hong-ping/ Dr Jessica Law',
            }
        ]
    },
    {
        show: true,
        tc_title: '員工天地',
        en_title: 'Staff Corner',
        prefix: 'staff-corner-',
        page: [
            {
                tc_name: '編輯委員會',
                en_name: 'Editorial Board',
            },
            {
                tc_name: '編輯及採訪',
                en_name: 'Editorial Team',
            }
        ]
    }
]

new Vue({
    el: '#app',
    components: {
        modal,
        share,
        counter
    },
    data: {
        lang: null,
        page: null,
        font_size: 1,
        mobile_nav: false,
        top: false,
        modal: false,
        video_modal: false,
        image: null,
        info: null,
        url: null,
        sharing: sharing,
        issue: issue,
        side: side,
        content: content,
        showing_content: null,
        year: null,
        modalElement: null,
        videoModalElement: null,
    },
    created() {
        window.addEventListener("resize", this.onResize)
        window.addEventListener('scroll', this.onScroll)
    },
    destroyed() {
        window.removeEventListener("resize", this.onResize)
        window.removeEventListener('scroll', this.onScroll)
    },
    mounted() {
        this.$cookies.config('7d', '/ehaslink', 'www3.ha.org.hk', true, 'strict')

        if (self == top) {
            // Everything checks out, show the page.
            document.documentElement.style.display = 'block'
        } else {
            // Break out of the frame.
            top.location = self.location
        }

        if (window.innerWidth > 991) {
            this.mobile_nav = true
            this.content.forEach((item, i) => {
                item.show = true
            })
        }

        let link = window.location.href.split('/')
        if (link[link.length - 1].includes(".html")) {
            this.lang = link[link.length - 2]
            this.page = link[link.length - 1]
        } else {
            if (link[link.length - 1] == '') {
                this.lang = link[link.length - 2]
            } else {
                this.lang = link[link.length - 1]
            }

            if (this.lang != "en" && this.lang != "tc") {
                this.lang = "tc"
            }

            window.location.replace(`../${this.lang}/index.html`);
        }

        let font_size = this.$cookies.get('font_size')

        if (font_size == null || font_size == '') {
            font_size = 1
        }


        this.onChangeFont(font_size)
        this.year = moment().format('YYYY')

        this.modalElement = document.querySelector('#modal')
        this.videoModalElement = document.querySelector('#video-modal')
    },
    methods: {
        onResize() {
            if (window.innerWidth <= 991) {
                this.mobile_nav = false
                this.content.forEach((item, i) => {
                    item.show = false
                })
            } else {
                this.mobile_nav = true
                this.content.forEach((item, i) => {
                    item.show = true
                })
            }
        },
        onChangeFont(size) {
            this.font_size = size
            this.$cookies.set('font_size', size)
            $("section .passage").css('font-size', 18 * percentage[size])
            $("section .passage h2").css('font-size', 18 * percentage[size])
            $("section .catalog .part").css('font-size', 18 * percentage[size])
        },
        onClickMenu(item, index) {
            if (window.innerWidth <= 991) {
                if (this.showing_content != null && index != this.showing_content && !item.show) {
                    this.content[this.showing_content].show = false
                }
                item.show = !item.show
                this.showing_content = index
            }
        },
        onScroll() {
            this.top = window.scrollY > window.innerHeight / 3
        },
        onScrollToTop() {
            window.scrollTo(0, 0)
        },
        onClickImage(image, info) {
            disableBodyScroll(this.modalElement)

            this.image = image
            this.info = info
            this.modal = true
        },
        onCloseModal() {
            enableBodyScroll(this.modalElement)

            this.modal = false
        },
        onClickVideo(url, info) {
            disableBodyScroll(this.videoModalElement)

            this.url = url
            this.info = info
            this.video_modal = true
        },
        onCloseVideoModal() {
            enableBodyScroll(this.videoModalElement)

            this.video_modal = false
        }
    },
})
