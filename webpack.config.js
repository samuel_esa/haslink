const path = require('path')
const fs = require('fs')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlWebpackPugPlugin = require('html-webpack-pug-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const FixStyleOnlyEntriesPlugin = require("webpack-fix-style-only-entries")
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const MinifyPlugin = require("babel-minify-webpack-plugin");

let templates = [];
let dir = 'src/pages/en';
let files = fs.readdirSync(dir);

files.forEach(file => {
  if (file.match(/\.pug$/)) {
    let filename = file.substring(0, file.length - 4);
    templates.push(
      new HtmlWebpackPlugin({
        template: dir + '/' + filename + '.pug',
        filename: './en/' + filename + '.html'
      })
    );
  }
})

dir = 'src/pages/tc';
files = fs.readdirSync(dir);

files.forEach(file => {
  if (file.match(/\.pug$/)) {
    let filename = file.substring(0, file.length - 4);
    templates.push(
      new HtmlWebpackPlugin({
        template: dir + '/' + filename + '.pug',
        filename: './tc/' + filename + '.html'
      })
    );
  }
})

module.exports = (env, options) => {
    console.log(`This is the Webpack 4 'mode': ${options.mode}`);
    if (options.mode == 'production') {
      return {
        mode: 'production',
        entry: {
          app: './src/index.js'
        },
        devtool: 'inline-source-map',
        devServer: {
          contentBase: './dist',
        },
        optimization: {
          minimize: true,
          minimizer: [new UglifyJsPlugin({
            include: /\.min\.js$/
          })]
        },
        plugins: [
          new CleanWebpackPlugin({ cleanStaleWebpackAssets: false }),
          ...templates,
          new HtmlWebpackPugPlugin(),
          new CopyPlugin(
            [
              { from: 'src/images/passage', to: 'images' },
              { from: 'src/images/sharing', to: 'images' },
              { from: 'src/backend', to: 'backend' }
            ]
          ),
          new VueLoaderPlugin(),
          new FixStyleOnlyEntriesPlugin(),
          new MiniCssExtractPlugin(),
          new OptimizeCssAssetsPlugin(),
          new MinifyPlugin()
        ],
        output: {
          filename: '[name].bundle.js',
          path: path.join(__dirname, 'dist')
        },
        module: {
          rules: [
            {
              test: /\.css$/,
              use: [
                MiniCssExtractPlugin.loader, //Extract css into files
                'css-loader', //turn css to commonjs
              ],
            },
            {
              test: /\.scss$/,
              use: [
                MiniCssExtractPlugin.loader, //Extract css into files
                'css-loader', //turn css to commonjs
                'sass-loader' // Turn sass into css
              ],
            },
            {
              test: /\.pug$/,
              oneOf: [
                // this applies to pug imports inside JavaScript
                {
                  exclude: /\.vue$/,
                  use: ['raw-loader', 'pug-plain-loader']
                },
                // this applies to <template lang="pug"> in Vue components
                {
                  use: ['pug-plain-loader']
                }
              ]
            },
            {
              test: /\.(png|svg|jpg|gif|jpe?g)$/,
              loader: 'file-loader',
              options: {
                outputPath: 'images',
              },
            },
            {
              test: /\.(woff|woff2|eot|ttf|otf)$/,
              use: [
                'file-loader',
              ],
            },
            {
              test: /\.vue$/,
              loader: 'vue-loader'
            },
            {
              test: /\.js$/,
              exclude: /node_modules/,
              loader: 'babel-loader',
              options: {
                presets: [
                  [
                    "@babel/preset-env",
                    {
                      useBuiltIns: 'usage'
                    }
                  ]
                ]
              }
            },
          ],
        },
        resolve: {
          alias: {
            vue: 'vue/dist/vue.esm.js'
          }
        }
      };
    } else {
      return {
        mode: 'development',
        entry: {
          app: './src/index.js'
        },
        devtool: 'inline-source-map',
        devServer: {
          contentBase: './dist',
        },
        plugins: [
          new CleanWebpackPlugin({ cleanStaleWebpackAssets: false }),
          ...templates,
          new HtmlWebpackPugPlugin(),
          new CopyPlugin(
            [
              { from: 'src/images/passage', to: 'images' },
              { from: 'src/images/sharing', to: 'images' }
            ]
          ),
          new VueLoaderPlugin(),
          new FixStyleOnlyEntriesPlugin(),
          new MiniCssExtractPlugin()
        ],
        output: {
          filename: '[name].bundle.js',
          path: path.join(__dirname, 'dist')
        },
        module: {
          rules: [
            {
              test: /\.css$/,
              use: [
                MiniCssExtractPlugin.loader, //Extract css into files
                'css-loader', //turn css to commonjs
              ],
            },
            {
              test: /\.scss$/,
              use: [
                MiniCssExtractPlugin.loader, //Extract css into files
                'css-loader', //turn css to commonjs
                'sass-loader' // Turn sass into css
              ],
            },
            {
              test: /\.pug$/,
              oneOf: [
                // this applies to pug imports inside JavaScript
                {
                  exclude: /\.vue$/,
                  use: ['raw-loader', 'pug-plain-loader']
                },
                // this applies to <template lang="pug"> in Vue components
                {
                  use: ['pug-plain-loader']
                }
              ]
            },
            {
              test: /\.(png|svg|jpg|gif|jpe?g)$/,
              loader: 'file-loader',
              options: {
                outputPath: 'images',
              },
            },
            {
              test: /\.(woff|woff2|eot|ttf|otf)$/,
              use: [
                'file-loader',
              ],
            },
            {
              test: /\.vue$/,
              loader: 'vue-loader'
            },
            {
              test: /\.js$/,
              exclude: /node_modules/,
            },
          ],
        },
        resolve: {
          alias: {
            vue: 'vue/dist/vue.esm.js'
          }
        }
      }
    }

}
